$(document).ready(function(){
  //to log the alt attributes of all images
  $("img").each( function() {
    console.log($(this).attr("alt"));
  });
  //traverse to the form and add class to it
  $(':button[value^="Color"]').click(function(){
    $('#searchField').parents('form')
      .addClass("formClass");
  });
  //select list item with class current and move this class to its next element
  $(':button[value="Move"]').click(function(){
    $('#myList li.current').removeClass("current")
      .next()
      .addClass("current");
  });
  //traversing to submit button
  $('#specials').change(function(){
    $(this).parent().find(':submit').css("background-color","red")
  });
  //set class=diabled for all the sibling elements of the first li child
  $(':button[value^="Spare"]').click(function(){
    $('#slideshow li:first-child').addClass("current").siblings()
      .addClass("disabled");
  });
});
$(document).ready(function() {
  //step1: hide all modules
  $('div.module').hide();
  //step5: show first tab
  $('div.module:first').addClass("current").show();
  //step2: create unordered list
  var ul = $('<ul></ul>')
  $('div.module h2').each(function() {
    var li = $('<li></li>').text($(this).text());
    ul.append(li);
  })
  //show tabs on page
  $('div#tabs').append(ul);
  //Step3: bind click event to li element(tabs)
  $('div#tabs ul li').on('click',function() {
    var clickedItem = $(this).text();
    $('div.module').each(function(){
      if ($(this).children('h2').text() == clickedItem) {
        //showing the related modules
        $(this)
          .addClass("current")
          .show()
            .siblings('div.module')
          .removeClass('current')
          .hide();
      }
    });
  });
});
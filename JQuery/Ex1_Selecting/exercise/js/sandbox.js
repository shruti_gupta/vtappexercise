$(document).ready(function(){
  //to select div with class as module
  $(':button[value^="Color"]').click(function(){
    $('div.module').css("background-color","yellow")
  });
  //to select the label of the search field(point 3)
  $('#searchField').click(function(){
    var searchId = $(this).attr('id');
    $('label[for='+ searchId +']').css("background-color","yellow");
  });
  //Ways to select 3rd li tag
  /*way1 (this is the best way to directly take out the 3rd child from the selector. 
  This is also better because we do not have to remember the indexes as in case of eq())
  */
  $(':button[value="Way1"]').click(function(){
    $('#myList li:nth-child(3)').css("background-color","yellow");
  });
  //way2
  $(':button[value="Way2"]').click(function(){
    $('#myList li').eq(2).css("background-color","red");
  });
  //way3
  $(':button[value="Way3"]').click(function(){
    $('#myList li:even').eq(1).css("background-color","blue");
  });
  //hidden element count
  $(':button[value^="Get"]').click(function(){
    $('#hiddenCount').val($(':hidden').length);
  });
  //alt attribute containing images
  $(':button[value="Images"]').click(function(){
    $('img[alt]').css("border","5px solid #f00");
  });
  //select odd rows of a table
  $(':button[value^="Odd"]').click(function(){
    $('tr:even').css("background-color","yellow");
  });
});
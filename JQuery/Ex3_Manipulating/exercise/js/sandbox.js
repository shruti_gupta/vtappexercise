$( document ).ready(function() {
  //to add 5 new li elements to the unordered list
  $( '#addElements' ).click(function() {
    for ( var i = 1; i < 6; i++ ) {
      $( '#myList' ).append( $( '<li/>' ).text( i ) );
    }
  });
  //remove odd li elements
  $( '#remove' ).click(function() {
    $( '#myList li:nth-child(2n + 1)' ).remove();
  });
  //add a paragraph and h2 to last div element
  $( '#addPara' ).click(function() {
    var $para = $( '<p/>', {
                    text: "helo this is the new paragraph"
                }),
        $heading = $( '<h2/>', {
                      text: "New Para Heading"
                    });
    $( 'div.module:last' ).append( $heading, $para );
  });
  //add missing wednesday to the select list
  $( '#addWednesday' ).click(function() {
     $( '<option/>', {
        value: "Wednesday",
        text: "Wednesday"
      }).insertAfter( $( '#week option[value="Tuesday"]' ) );
  });
  //create a new div and add it after the last div with a copy of existing text
  $( '#addDiv' ).click(function() {
    $( '<div/>', {
        addClass: "module"
      })
    .insertAfter( $( 'div.module:last' ) )
    .append( $( '#myList' ).clone());
  });
});
$(document).ready(function() {
  var searchInputDOM = $( '#searchField' );
  var label = searchInputDOM.siblings( 'label[for="'+ searchInputDOM.attr("id") +'"]' );
  //step1 + step2
  var hintText = label.text();
  setSearchField( hintText );
  //step3: remove label
  label.remove();
  //step4 : bind a focus eventand remove class 'hint'
  searchInputDOM.on( "focus", function(){
    if ($( this ).val() == hintText && $( this ).hasClass( "hint" )) {
      $( this ).val( "" ).removeClass( "hint" );
    }
  });
  //step5: bind blur event to revert the search field if no input given
  searchInputDOM.on("blur",function(){
    if ($( this ).val().trim().length == 0) {
      setSearchField( hintText );
    }
  });
});
var setSearchField = function( hintText ){
  $( '#searchField' ).val(function() {
    $( this ).addClass( "hint" ); 
    return hintText
  });
}
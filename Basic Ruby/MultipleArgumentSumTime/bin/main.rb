require_relative '../lib/custom_time'

time_array = []

puts "Enter time value in (h:m:s) format (Press Q/q to exit)"
until ( time_string = gets.chomp ) =~ /^q$/i
  time = CustomTime.new(time_string)
  if time.valid?
    time_array << time
  else
    puts "Invalid Time !!! This will not be included in time addition"
  end
  puts "Enter time value in (h:m:s) format (Press Q/q to exit)"
end

first_time = time_array.pop

if !time_array.length.zero?
  time = first_time.sum(time_array)
  TimeOperations::display_time(time)
else
  puts "Enter atleast one value"
end
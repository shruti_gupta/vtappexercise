require_relative 'time_operations'

class CustomTime
  include TimeOperations

  attr_reader :time
  def initialize(timestring)
    @time = timestring
  end

  def sum(time_array)
    cumulative_time = 0
    if !time_array.length.zero?
      time_array.each do |time_object|
        cumulative_time += time_object.convert_to_seconds
      end
    end
    total_secs = convert_to_seconds + cumulative_time
    convert_seconds_to_time(total_secs)
  end

end
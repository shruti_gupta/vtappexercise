module TimeOperations

  VALID_TIME = /^(([0-1]\d)|(2[0-3])|(\d)):(([0-5]\d)|(\d)):(([0-5]\d)|(\d))$/

  def valid?
    VALID_TIME =~ time ? true : false
  end
  
  def convert_to_seconds
    hours, mins, secs = time.split(":")
    (hours.to_i * 3600) + (mins.to_i * 60) + (secs.to_i)
  end

  def convert_seconds_to_time(total_secs)
    interim_hours, interim_min = total_secs.divmod(3600)
    days, hours = interim_hours.divmod(24)
    minutes, seconds = interim_min.divmod(60)
    {"day"=>days, "hour"=>'%02d' % hours, "minute"=>'%02d' % minutes, "second"=>'%02d' % seconds}
  end

  def self.display_time(time)
    if time["day"].eql? 0
      puts "#{ time["hour"] }:#{ time["minute"] }:#{ time["second"] }"
    else
      puts "#{ time["day"] }day & #{ time["hour"] }:#{ time["minute"] }:#{ time["second"] }"
    end
  end
  
end
class Customer
  @@bank_accounts = 1234

  def initialize( name )
    @name = name
    @@bank_accounts += 1
    @account_no = @@bank_accounts
    @balance = 1000
  end

  def deposit( amount )
    @balance += amount
  end

  def withdrawl( amount )
    @balance -= amount
  end

  def to_s
    puts " #{@name} with #{@account_no} and have #{@balance} rupees in account"
  end

end
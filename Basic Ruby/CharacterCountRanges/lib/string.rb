class String

  def count_characters_types
    hash = Hash.new(0)
    self.each_byte do |byte|
      case byte
      when 48..57
        hash["numbers"] += 1
      when 65..90
        hash["uppercase"] += 1
      when 97..122
        hash["lowercase"] += 1
      else
        hash["special"]   += 1
      end
    end
    hash
  end

end
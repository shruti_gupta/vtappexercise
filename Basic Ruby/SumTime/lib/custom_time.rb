class CustomTime

  VALID_TIME = /^(([0-1]\d)|(2[0-3])|(\d)):(([0-5]\d)|(\d)):(([0-5]\d)|(\d))$/

  def initialize(timestring)
    @time = timestring
  end

  def valid?
    VALID_TIME =~ @time ? true : false
  end
  
  def convert_to_seconds
    hours, mins, secs = @time.split(":")
    (hours.to_i * 3600) + (mins.to_i * 60) + (secs.to_i)
  end

  def convert_seconds_to_time(compound_seconds)
    interim_hours, interim_min = compound_seconds.divmod(3600)
    days, hours = interim_hours.divmod(24)
    minutes, seconds = interim_min.divmod(60)
    {"day"=>days, "hour"=>'%02d' % hours, "minute"=>'%02d' % minutes, "second"=>'%02d' % seconds}
  end

  def sum(time2)
    compound_seconds = convert_to_seconds + time2.convert_to_seconds
    convert_seconds_to_time(compound_seconds)
  end

end
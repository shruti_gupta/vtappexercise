require_relative '../lib/custom_time'

puts "Enter first time value in (h:m:s) format"
time1 = CustomTime.new(gets.chomp)
puts "Enter second time value in (h:m:s) format"
time2 = CustomTime.new(gets.chomp)

if time1.valid? && time2.valid?
  time = time1.sum(time2)
  if time["day"].eql? 0
    puts "#{ time["hour"] }:#{ time["minute"] }:#{ time["second"] }"
  else
    puts "#{ time["day"] }day & #{ time["hour"] }:#{ time["minute"] }:#{ time["second"] }"
  end
else
  puts "Invalid time"
end
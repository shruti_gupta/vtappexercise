require_relative '../../../MultipleArgumentSumTime/lib/time_operations'

class CustomTime
  include TimeOperations
  attr_reader :time

  def initialize(timestring)
    @time = timestring
  end

  def sum(time2)
    compound_seconds = convert_to_seconds + time2.convert_to_seconds
    convert_seconds_to_time(compound_seconds)
  end

end
require_relative '../lib/custom_time'

puts "Enter first time value in (h:m:s) format"
time1 = CustomTime.new(gets.chomp)
puts "Enter second time value in (h:m:s) format"
time2 = CustomTime.new(gets.chomp)

if time1.valid? && time2.valid?
  time = time1.sum(time2)
  TimeOperations::display_time(time)
else
  puts "Invalid time"
end
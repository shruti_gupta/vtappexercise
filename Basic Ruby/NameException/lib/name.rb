require_relative 'nil_exception'
require_relative 'improper_case_exception'

class Name

  def initialize(firstname, lastname)
    @firstname = firstname
    @lastname = lastname
    valid?
  end

  def valid?
    if @firstname.empty?
      raise NilException.new "Firstname cannot be empty"
    elsif @lastname.empty?
      raise NilException.new "Lastname cannot be empty"
    elsif @firstname !~ /^[A-Z]/
      raise ImproperCaseException.new "First Letter of the firstname should be capital"
    end
  end
  private:valid?

  def to_s
    "Hello #{@firstname} #{@lastname}"
  end

  
end
class Array

  def reverse_iterate
    for j in (1..length)
      yield self[-j]
    end
  end 

end
class Dictionary
  @@words = ["Testingg123", "tesTing23542", "asdtest", "sazwqa"]

  def self.select_from_dictionary(word)
    to_be_searched = Regexp.new("^#{word}", 'i')
    @@words.select { |word| word =~ to_be_searched }
  end

  def self.search(word)
    suggestions = select_from_dictionary(word).sort

    if suggestions.empty?
      @@words.push(word).sort
      "No such word present but added this new word in the dictionary"
    else
      return suggestions
    end
  end
end
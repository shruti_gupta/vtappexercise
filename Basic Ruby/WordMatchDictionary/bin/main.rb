require_relative '../lib/dictionary'

answer = "y"

while answer =~ /^y$/i
  puts "Enter word to be searched in the dictionary"
  word = gets.chomp
  puts "Suggestions: #{Dictionary.search(word)}"
  puts "Do you want to search more words ? (y/n) :"
  answer = gets.chomp
end
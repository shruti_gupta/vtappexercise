class Fibonacci

  def initialize( number )
    @number = number
  end

  def print
    first, second = 0, 1
    yield( first )
    1.upto( @number ) do
      first, second = second, first + second
      yield( second )
    end
  end
  
end
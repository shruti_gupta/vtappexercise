require_relative '../lib/product'
require_relative '../lib/bill'

product_array = []
answer = "y"

while answer =~ /^y$/i do
  print "Name of the Product :"
  product = gets.chomp
  print "Imported? (y/n) :"
  imported = gets.chomp
  print "Exempted from sales tax ?(y/n) :"
  tax_exemption = gets.chomp
  print "Price:"
  price = gets.chomp
  product_array.push(Product.new(product, imported, tax_exemption, price))
  print "Do you want to add more items to your list ?(y/n) :"
  answer = gets.chomp
end

Bill.new(product_array).print
class Bill

  def initialize(chosen_products)
    @billable_products = chosen_products
    @total = 0
  end

  def update_price
    @billable_products.each do |product|
      product.price *= ( calc_sales_tax(product) + calc_import_tax(product) )
    end
  end

  def calc_sales_tax(product)
    (product.tax_exemption =~ /^y$/i) ? 1 : 0.1 * product.price
  end

  def calc_import_tax(product)
    (product.imported =~ /^n$/i) ? 1 : 0.05 * product.price
  end

  def calc_total
    @billable_products.each do |product|
      @total += product.price
    end
  end

  def prepare_bill
    update_price
    calc_total
  end

  def print
    prepare_bill

    (1..50).step { printf("%s", '_') }
    puts ""
    puts "**************Thakyou for Shopping****************"
    (1..50).step { printf("%s", '_') }
    puts ""
    @billable_products.each do |product|
      puts "#{product.name}..........#{product.price}"
    end
    (1..50).step { printf("%s", '_') }
    puts ""
    puts "*******************Grand Total********************"
    (1..50).step { printf("%s", '_') }
    puts ""
    puts @total.round(2)
    (1..50).step { printf("%s", '_') }
    puts ""
  end

end
class Product
  attr_reader :name, :imported, :tax_exemption  
  attr_accessor :price 

  def initialize(product_name, imported, tax_exemption, price)
    @name = product_name
    @imported = imported
    @tax_exemption = tax_exemption
    @price = price.to_f
  end

end
class Grammer
  def self.pluralize(count, word)
    count > 1 ? word + 's' : word
  end
end
require 'csv'
require_relative 'grammer'
require_relative 'employee'

class EmployeeFile

  def initialize(input_filepath, output_filename = 'output.txt')
    @input_filepath = input_filepath
    @output_filepath = "../output/#{output_filename}"
    @employees_details = Hash.new { |hash, key| hash[key] = [] }
  end

  def create
    data = read
    write(data)
  end

  private
    def read
      CSV.foreach(@input_filepath, headers: true, converters: :integer, header_converters: :downcase) do |employee_detail|
        @employees_details[employee_detail['designation']] << Employee.new(employee_detail)
      end
      @employees_details
    end

    def write(data)
      File.open(@output_filepath, 'w') do |file|
        @employees_details.each do |designation, employees|
          file.puts "\n"
          file.puts Grammer.pluralize(employees.count, designation)
          file.puts employees.sort_by(&:id)
        end
      end 
    end

end
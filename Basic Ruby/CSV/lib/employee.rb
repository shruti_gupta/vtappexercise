class Employee

  attr_reader :id, :name, :designation

  def initialize(detail)
    @name = detail['name']
    @id = detail['empid']
    @designation = detail['designation']
  end

  def to_s
    " #{@name}(Emp Id:#{@id})"
  end
end
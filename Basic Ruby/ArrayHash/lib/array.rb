class Array

  def to_hash
    hash = Hash.new { |hash, key| hash[key] = [] }
    for element in self do

      index = (element.is_a? Numeric) ? element.to_s.size : element.size
      hash[ index ] << element
    end
    hash
  end

end
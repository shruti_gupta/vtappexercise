require_relative '../lib/integer'

begin
  puts "Enter a number to find its factorial"
  number = gets.chomp
  puts "The factorial is #{Integer(number).factorial}"
rescue LessThanZeroException
  puts "#{$!} Try again!!!"
  retry
rescue ArgumentError
  puts "#{$!} Try again!!!"
  retry
end
class LessThanZeroException < StandardError; end

class Integer

  def factorial
    output = 1
    if self < 0
      raise LessThanZeroException.new "Number cannot be less than zero"
    elsif self == 0
      1
    else
      for i in (1..self)
        output *= i
      end
    end
    output
  end

end
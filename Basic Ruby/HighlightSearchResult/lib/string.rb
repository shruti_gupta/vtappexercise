class String

  def search(word)
    occurance_count = 0
    string = gsub( /#{ Regexp.escape(word) }/i ) do |matched_word|
      occurance_count += 1
      "(#{ matched_word })"
    end
    return string, occurance_count
  end

end
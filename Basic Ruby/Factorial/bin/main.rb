require_relative '../lib/numeric'

puts "Enter a number to find its factorial"
number = gets.chomp
puts "The factorial is #{Integer(number).factorial}"
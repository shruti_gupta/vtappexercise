class Integer

  def factorial
    if self < 0
      "<<ERROR!!Cannot calculate factorial of a negetive Integer>>"
    elsif self == 0
      1
    else
      (1..self).inject(:*)
    end
  end

end
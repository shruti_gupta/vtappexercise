class Array

  def to_grouped_hash
    group_by { |element| element.to_s.size }
  end

  def seperate_into_even_odd_keys
    hash = Hash.new { |hash, key| hash[key] = [] }
    to_grouped_hash.inject(hash) do |memo, (key, value)|
      key.even? ? memo[ "even" ] << value : memo[ "odd" ] << value
      memo
    end
  end
end
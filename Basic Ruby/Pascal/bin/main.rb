require_relative '../lib/pascal_triangle'
require_relative '../../FactorialException/lib/integer'

begin
  puts "Enter the level of the pascal's triangle you want to see"
  level = Integer(gets.chomp)
  raise LessThanZeroException.new ("Level cannot be less than zero") if level < 0
  PascalTriangle.generate(level) do |n|
    (n + 1).times do |k|
      x = n.factorial / ( k.factorial * (n - k).factorial )
      printf("%d ", x)
    end
    puts "\n"
  end
rescue LessThanZeroException
  puts "#{$!} Try Again!!"
  retry
rescue ArgumentError
  puts "#{$!} Try Again!!"
  retry
end
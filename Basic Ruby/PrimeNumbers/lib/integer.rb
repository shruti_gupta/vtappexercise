require 'prime'

class Integer

  def primes_till
    prime_numbers = []
    if self >= 2
      prime_numbers << 2 << 1.step(self, 2).select { |number| number.prime? }
    end
    prime_numbers
  end

end
require_relative '../lib/integer'

puts "Enter a number upto which you want to see the prime numbers"
number = gets.chomp

if number =~ /^[+]?\d+$/
  puts number.to_i.primes_till.join(" ")
else
  puts "Enter only integers greater than 0"
end
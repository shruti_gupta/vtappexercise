require_relative '../lib/interest_calculator'

puts "Enter principal"
principal = gets.chomp.to_f

puts "Enter time in years"
time = gets.chomp.to_i

interest_calculator = InterestCalculator.new(principal,time) { |difference| puts "The difference is = #{difference}"}
interest_calculator.get_interest_difference
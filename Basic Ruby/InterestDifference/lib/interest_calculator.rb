class InterestCalculator

  def initialize(principal, time, rate=10, &block)
    @principal = principal
    @time = time
    @rate = rate
    @block = block
  end

  def get_si_amount
    @principal * @rate * @time / 100 + @principal
  end

  def get_ci_amount
    @principal * (1 + ((@rate / 100) ** @time))
  end

  def get_interest_difference
    @block.call(get_si_amount.to_f - get_ci_amount.to_f)
  end

end
require_relative 'string'

puts "Enter a string to see if its palindrome (Press Q/q to exit)"
until ( input_string = gets.chomp ) =~ /^q$/i
  if input_string.palindrome?
    puts "It is a palindrome"
  else
    puts "It is not a palindrome"
  end
end
class String

  def palindrome?
    self.eql? self.reverse
  end

end
var jsonObject  = [{"name":"Luigi Damiano"},
                  {"name":"Zenith Coboro"},
                  {"name":"Zig Ziglar"},
                  {"name":"Steve Costner"},
                  {"name":"Bill Grazer"},
                  {"name":"Timothy Frazer"},
                  {"name":"Boris Becker"},
                  {"name":"Glenn Gladwich"},
                  {"name":"Jim Jackson"},
                  {"name":"Aaron Kabin"},
                  {"name":"Roy Goldwin"},
                  {"name":"Jason Goldberg"},
                  {"name":"Tim Ferris"},
                  {"name":"Buck Singham"},
                  {"name":"Malcom Gladwell"},
                  {"name":"Joy Rabura"},
                  {"name":"Vid Luther"},
                  {"name":"Tom Glicken"},
                  {"name":"Ray Baxter"},
                  {"name":"Ari Kama"},
                  {"name":"Kenichi Suzuki"},
                  {"name":"Rick Olson"}]

function Form(inputField, jsonObject){

  this.inputField = document.getElementById(inputField);
  this.mainDiv = this.inputField.parentNode.parentNode;
  this.resultDiv = document.createElement("div");
  this.resultDiv.setAttribute("class", "resultDiv");
  this.jsonObject = jsonObject;

  this.handleKeyupEvent = function(){
    var form = this;
    this.inputField.onkeyup = function(){
      form.autocompleteInputfield(this.value);
    }
  }

  this.autocompleteInputfield = function(characters){
    this.matchCharacters(characters);
    this.printResult();
    this.handleClickEvent();
  }

  this.handleClickEvent = function(element){
    var form = this;
    document.getElementById("ulElement").onclick = function(e){ //event bubbling
      var target = e.target || e.srcElement;
      if(target.nodeName == 'LI'){
        form.inputField.value = target.innerText;
      }
    }
  }

  this.matchCharacters = function(characters){
    var ul = document.createElement('ul');
    ul.setAttribute("id", "ulElement");
    this.resultDiv.innerText = "";
    var regex = new RegExp(characters, 'i');
    for(var i = 0; i < this.jsonObject.length; i++){
      if(this.jsonObject[i].name.match(regex)){
        this.appendToList(ul, this.jsonObject[i].name);
      }
    }
    this.resultDiv.appendChild(ul);
  }

  this.appendToList = function(ul, name){
    var li = document.createElement('li');
    li.setAttribute("class","listElement");
    li.innerText = name;
    ul.appendChild(li);
  }

  this.printResult = function(){
    var position = this.getElementPosition();
    this.resultDiv.style.position = "absolute";
    this.resultDiv.style.left = position[0] + "px";
    this.resultDiv.style.top = position[1] + this.inputField.offsetHeight + "px";
    this.mainDiv.appendChild(this.resultDiv);
  }

  this.getElementPosition = function(){
    var inputLeft = inputTop = 0;
    var obj = this.inputField;
    if(obj.offsetParent) {
      do {
        inputLeft += obj.offsetLeft;
        inputTop += obj.offsetTop;
      } while (obj = obj.offsetParent);
    }
    return [inputLeft, inputTop];
  }
}
window.onload = function(){
  var inputField = new Form("name", jsonObject);
  inputField.handleKeyupEvent();
}
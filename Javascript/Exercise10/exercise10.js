function Form(){
  this.jsform = document.getElementById("myForm");
  this.notificationCheckbox = document.getElementById("notificationCheckbox");
  this.textareaElement = document.getElementById("aboutme");
  this.allformElements = this.jsform.elements;
  this.handleOnSubmit();
}

Form.prototype.handleOnSubmit = function(){
  form = this;
  this.jsform.onsubmit = function(){
    return form.validateOnSubmit();
  }
  this.allformElements.HomepageURL.onblur = function(){
    form.validateURL(this);
  }
  this.allformElements.Email.onblur = function(){
    form.validateEmail(this);
  }
}

Form.prototype.validateOnSubmit = function(){
  return(this.checkEmptyFields() && this.checkTextareaLength() && this.checkForNotification());
}

Form.prototype.validateEmail = function(object){
  var pattern = new RegExp(/(\w)+@\w+\.\w+/);
  if(!pattern.test(object.value)){
    alert("Please enter a valid Email address with @ and .")
    object.focus();
  }
}

Form.prototype.validateURL = function(object){
  var pattern = new RegExp(/^((http|ftp)(s?)\:\/\/)?([wW]{3}\.)?(?:\w+\.)+\w+(.*)$/);
  if(!pattern.test(object.value)){
    alert("Please enter a valid URL either starting with http:// or www")
    object.focus();
  }
}

Form.prototype.checkEmptyFields = function(){
    for(var i = 0; i < this.allformElements.length; i++){
    if(this.allformElements[i].value.length == 0){
      alert(this.allformElements[i].name + " field can't be empty");
      return false;
    }
  }
  return true;
}

Form.prototype.checkForNotification = function(){
  if(this.notificationCheckbox.checked == false){
    alert("Please check the checkbox to receive notification");
    return false;
  }
  return true;
}

Form.prototype.checkTextareaLength = function(){
  if(this.textareaElement.value.length > 50){
    alert("Length of " + this.textareaElement.name + " field can't exceed 50 characters");
    return false;
  }
  return true;
}
 var validateForm = new Form();
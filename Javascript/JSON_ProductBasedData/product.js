var jsonObject = [{"name":"1" , "url":"1.jpg" , "color":"Yellow" , "brand":"BRAND A" , "sold_out":"1"} , 
                    {"name":"2" , "url":"2.jpg" , "color":"Red" , "brand":"BRAND B" , "sold_out":"0"} , 
                    {"name":"3" , "url":"3.jpg" , "color":"Green" , "brand":"BRAND D" , "sold_out":"0"} , 
                    {"name":"4" , "url":"4.jpg" , "color":"Red" , "brand":"BRAND A" , "sold_out":"1"} , 
                    {"name":"5" , "url":"5.jpg" , "color":"Blue" , "brand":"BRAND B" , "sold_out":"0"} , 
                    {"name":"6" , "url":"6.jpg" , "color":"Green" , "brand":"BRAND C" , "sold_out":"0"} , 
                    {"name":"7" , "url":"7.jpg" , "color":"Red" , "brand":"BRAND C" , "sold_out":"1"} , 
                    {"name":"8" , "url":"8.jpg" , "color":"Blue" , "brand":"BRAND D" , "sold_out":"0"} , 
                    {"name":"9" , "url":"9.jpg" , "color":"Yellow" , "brand":"BRAND A" , "sold_out":"0"} , 
                    {"name":"10" , "url":"10.jpg" , "color":"Yellow" , "brand":"BRAND B" , "sold_out":"1"} , 
                    {"name":"11" , "url":"11.jpg" , "color":"Green" , "brand":"BRAND D" , "sold_out":"0"} , 
                    {"name":"12" , "url":"12.jpg" , "color":"Yellow" , "brand":"BRAND D" , "sold_out":"0"} , 
                    {"name":"13" , "url":"13.jpg" , "color":"Blue" , "brand":"BRAND A" , "sold_out":"0"} , 
                    {"name":"14" , "url":"14.jpg" , "color":"Blue" , "brand":"BRAND D" , "sold_out":"0"} , 
                    {"name":"15" , "url":"15.jpg" , "color":"Green" , "brand":"BRAND B" , "sold_out":"0"} , 
                    {"name":"16" , "url":"16.jpg" , "color":"Yellow" , "brand":"BRAND B" , "sold_out":"1"} , 
                    {"name":"17" , "url":"17.jpg" , "color":"Green" , "brand":"BRAND A" , "sold_out":"1"} , 
                    {"name":"18" , "url":"18.jpg" , "color":"Blue" , "brand":"BRAND D" , "sold_out":"1"} , 
                    {"name":"19" , "url":"19.jpg" , "color":"Green" , "brand":"BRAND C" , "sold_out":"0"} , 
                    {"name":"20" , "url":"20.jpg" , "color":"Yellow" , "brand":"BRAND A" , "sold_out":"0"}]

function Product(jsonObject){
  this.div = document.getElementById("bodydiv");
  this.sortList = document.getElementById("sortlist");
  this.jsonObject = jsonObject;

  this.handleOnChange = function(){
    var productList = this;
    this.sortList.onchange = function(){
      productList.printOnDiv(this.value);
    }
  }

  this.sortByProperty = function(property) {
    return function (a, b) {
      var sortStatus = 0;
      if (a[property] < b[property]) {
          sortStatus = -1;
      } else if (a[property] > b[property]) {
          sortStatus = 1;
      }
      return sortStatus;
    };
  }
  
  this.printOnDiv = function(property){
    this.jsonObject.sort(this.sortByProperty(property));
    this.div.innerHTML = "";
    for(var i = 0; i < this.jsonObject.length; i++){
      this.showBrandIcons(this.jsonObject[i].url);
    }
  }

  this.showBrandIcons = function(url){
    imgObject = document.createElement("img");
    imgObject.setAttribute("class", "brandIcon");
    imgObject.setAttribute("src", "./images/" + url + "");
    this.div.appendChild(imgObject);
  }
}
window.onload = function(){
  var productList = new Product(jsonObject);
  productList.handleOnChange();
}
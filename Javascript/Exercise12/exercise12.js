function URL(){
  this.jsform = document.getElementById("form");
  this.pattern = new RegExp(/^(?:(?:(?:ht|f)tp(?:s?)\:\/\/)?(?:www\.)?)?(?:((?:.+\.)*(?:.*))\.)?((?:.+){1}\.(?:.{1,3})){1}(.*)$/);
  this.resultDiv = document.getElementById("resultDiv");
  this.eventListener = function(){
    var url = this;
    this.jsform.elements.button.onclick = function(){
      url.getDomainSubDomainName();
    }
  }
  this.getDomainSubDomainName = function(){
    var answer;
    if(this.jsform.elements.urlTextField.value.match(this.pattern)){
      answer = "Domain : " + RegExp.$2 + "\t\t\t" + "Subdomain : " + RegExp.$1;
    }else{
      answer = "Invalid URL"
    }
    this.printOnHTML(answer); 
  }
  this.printOnHTML = function(answer){
    this.resultDiv.innerText = answer;
  }
}

window.onload = function(){
  var url = new URL();
  url.eventListener(); 
}
function Text(){
  this.jsform = document.getElementById("form");
  this.pattern = new RegExp(/^[-+]?\d+((\.)\d+)?$/);

  this.handleOnSubmit = function(){
    var text = this;
    this.jsform.onsubmit = function(){
      return text.isNumber(this);
    }
  }

  this.isNumber = function(object){ 
    if(! this.pattern.test(object.elements.textInput.value)){
      object.elements.resultField.value = "false";
      return false;
    }
    object.elements.resultField.value = "true";
    return true;
  }
}
var number = new Text();
number.handleOnSubmit();
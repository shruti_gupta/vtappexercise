function Table(colsize, tableId) { 
  this.colSize = colsize;
  this.buttonRowId = document.getElementById(tableId);
  this.button = this.buttonRowId.getElementsByTagName('input')[0];
}

Table.prototype.handleClickEvent = function(){
  var table = this;
  this.button.onclick = function(){
    table.createRow();
  } 
}  

Table.prototype.createRow = function(){
  var row = new Row();
  row.table = this;
  row.init();
}

function Row() {
  this.table = "";

  this.init = function() {
    var button = this.table.buttonRowId;
    var array = ["", ""];
    var newRow = this.createRow(array, this.table.button);
    button.parentNode.insertBefore(newRow, button);
  },
  
  this.saveOrEditRow = function() {
    var currentRow = this.parentNode.parentNode;
    var array;
    if(this.id == "Save"){
      array = [currentRow.getElementsByClassName("name")[0].value, currentRow.getElementsByClassName("email")[0].value];
    }else{
      array = [currentRow.getElementsByClassName("name")[0].innerText, currentRow.getElementsByClassName("email")[0].innerText];
    }
    var newRow = cell.row.createRow(array, this);
    currentRow.parentNode.replaceChild(newRow, currentRow);
  }

  this.deletRow = function(){
    this.parentNode.parentNode.remove(0);
  }

  this.createRow = function(array, object){
    var newRow = document.createElement('tr');
    newRow = this.createColumns(newRow, object, array);
    return newRow;
  },
  
  this.createColumns = function(newRow, object, array){
    var newCell;
    var columns = ["name", "email"]
    for(var i = 0; i < this.table.colSize - 1; i++){
      newCell = newRow.insertCell(i);
      newCell.appendChild(this.addElement(object, array[i], columns[i]));
    }
    newCell = newRow.insertCell(this.table.colSize - 1);
    cell.row = this;
    if(object.id == "addrowbutton" || object.id == "Edit"){
      newCell.appendChild(cell.getAnchor("Save", this.saveOrEditRow));
    }else{
      newCell.appendChild(cell.getAnchor("Edit", this.saveOrEditRow));
      newCell.appendChild(cell.getAnchor("/ Delete ", this.deletRow));
    }
    return newRow;
  },

  this.addElement = function(object, value, cellName){
    var field;
    if(object.id == "addrowbutton" || object.id == "Edit"){
      field = cell.getInputType(value, cellName);
    }else{
      field = cell.getLabel(value, cellName);
    }
    return field;
  }
}

cell = {
  row: "",
  getInputType: function(value, cellName) {
    var field = document.createElement('input');
    field.type = "text";
    field.value = value;
    field.setAttribute("class",cellName);
    return field;
  },

  getLabel: function(labelValue, cellName) {
    var label = document.createElement('label');
    label.value = labelValue;
    label.innerText = labelValue;
    label.setAttribute("class",cellName);
    return label;
  },

  getAnchor: function(innerText, action) {
    var anchor = document.createElement('a');
    anchor.onclick = action;
    anchor.href = "#";
    anchor.id = innerText;
    anchor.innerText = innerText;
    return anchor;
  }
}
window.onload = function(){
  var table = new Table(3, "rowadd");
  table.handleClickEvent(); 
}
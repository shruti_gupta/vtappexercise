var validate = new Validation(); 

function User(name, age) {
  this.name = name;
  this.age = age;
}
User.prototype.compare = function(otherUser) {
  var result = null;
  if (this.age > otherUser.age) {
    result = this.name + " is older than " + otherUser.name;
  } else if (this.age < otherUser.age) {
    result = otherUser.name + " is older than " + this.name;;
  } else {
    result = this.name + " and " + otherUser.name + " are of same age";
  }
  document.getElementById("result").innerHTML = result;
}

function Validation(){
  this.validateField = function(object){
    if (isNaN(object.value) || (object.value < 0)) {
    alert("Please enter a valid age");
    object.value = "";
    object.focus();
    }
  }
}

function creatingUsers() {
  user1 = new User(document.getElementById('username1').value, document.getElementById('userage1').value);
  user2 = new User(document.getElementById('username2').value, document.getElementById('userage2').value);
  user1.compare(user2);
}
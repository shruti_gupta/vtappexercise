function GoToURL(){
  this.userUrl = this.getURL();
  this.openURL(this.userUrl);
}

GoToURL.prototype.getURL = function(){
  do{
    this.userURL = prompt("Enter a URL");
  }while(this.validateURL(this.userURL))
  return this.userURL.trim();
}

GoToURL.prototype.validateURL = function(userUrl){
    var pattern = new RegExp(/^((http|ftp)(s?)\:\/\/)?(([wW]{3})\.)?(?:\w+\.)+\w+(.*)$/);
    if(! pattern.test(userUrl)){
    alert("Enter a valid URL");
    return true;
  }
  return false;
}

GoToURL.prototype.openURL = function(userUrl){
  window.open(userUrl ,"", "width=400, height=400, status=no, titlebar=no, scrollbars=no, menubar=no, location=no");
}

var myurl = new GoToURL();
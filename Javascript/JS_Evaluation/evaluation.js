function Form(form, table){
  this.inputForm = document.getElementById(form);
  this.form = this.inputForm.elements;
  this.name = "";
  this.age = "";
  this.table = new Table(table);

  this.handleEvents = function(){
    var formRecord = this;
    this.form.submitButton.onclick = function(){
      formRecord.name = formRecord.form.name.value;
      formRecord.age = formRecord.form.age.value;
      if(formRecord.validateEntries()){
        formRecord.inputForm.reset();
        formRecord.insertIntoTable();
      }
    } 
  }

  this.insertIntoTable = function(){
    this.table.insertRow({name: this.name, age: this.age});
  }

  this.validateEntries = function(){
    if(validate.presence(this.name)){
      alert("Please enter a valid name");
      return false;
    }
    if(validate.presence(this.age) || validate.isNumber(this.age)){
      alert("Please enter a valid age");
      return false;
    }
    return true;
  }
}

validate = {
  presence: function(value){
    return (value.trim().length == 0)
  },
  isNumber: function(value){
    return (isNaN(value))
  }
}

function Search(table, searchField){
  this.records = new Records();
  this.table = document.getElementById(table);
  this.rows = this.table.getElementsByTagName('tr');

  this.handleKeyupEvent = function(){
    var search = this;
    document.getElementById(searchField).onkeyup = function(){
      search.hideRows(search.getNonMatchingRecords(this.value));
    }
  }

  this.getNonMatchingRecords = function(characters){
    var nonMatchingRecords = new Array();
    var regex = new RegExp(characters, 'i');
    for(var i = 0; i < this.records.recordsList.length; i++){
      this.rows[i].style.display = "";
      var string = [];
      currentRecord = this.records.recordsList[i]
      for(key in currentRecord){
        string += currentRecord[key];
      }
      if(!(string.match(regex))){
        nonMatchingRecords.push(currentRecord.rowId);
      }
    }
    return nonMatchingRecords;
  }

  this.hideRows = function(nonMatchingRecords){
    for(var i = 0; i < nonMatchingRecords.length; i++){
      document.getElementById(nonMatchingRecords[i]).style.display = "none";
    }
  }
}

function  Table(table){
  this.colSize = 3;
  this.tableId = document.getElementById(table);
  this.records = new Records();

  this.insertRow = function(jsonData){
    var id = this.tableId.firstChild.childElementCount;
    var row = new Row(id);
    row.table = this;
    this.records.row = row;
    this.records.makeRecord(id, jsonData);
  }
}
Table.prototype.recordsList = [];

function Record(id, name, age){
  this.rowId = id;
  this.name = name;
  this.age = age;
}

function Records(){
  this.row = "";

  this.makeRecord = function(id, jsonData){
    var newRecord = new Record(id) //creating a record Object
    for(key in jsonData){
      newRecord[key] = jsonData[key]
    }
    this.recordsList.push(newRecord);
    this.createDomRow(newRecord);
  }

  this.createDomRow = function(newRecord){
    this.row.create(newRecord);
  }

  this.removeRecord = function(rowID){
    var index;
    for(var i = 0; i < this.recordsList.length; i++){
      if(this.recordsList[i]["rowId"] == Number(rowID.id)){
        index = i;
        break;
      }
    }
    this.recordsList.splice(index, 1);
    this.removeDomRow(rowID);
  }

  this.removeDomRow = function(rowID){
    rowID.remove(0);
  }
}
Records.prototype.recordsList = [];

function Row(id){
  this.table = "";

  this.create = function(recordData){
    var newRow = this.table.tableId.insertRow(0);
    newRow.id = id;
    this.createColumns(newRow, recordData);
  }

  this.createColumns = function(newRow, recordData){
    var cell = new Cell();
    cell.row = this;
    var cellCounter = 0
    for(key in recordData){
      if(key == "rowId") { continue; }
      newCell = newRow.insertCell(cellCounter)
      newCell.appendChild(cell.getLabel(recordData[key]))
      cellCounter++
    }
    newCell = newRow.insertCell(this.table.colSize - 1);
    newCell.appendChild(cell.getAnchor());
  }

  this.deletRow = function(){
    this.row.table.records.removeRecord(this.parentNode.parentNode);
  }
}

function Cell(){
  this.row = "";
}
Cell.prototype.getLabel = function(labelValue) {
  var label = document.createElement("label");
  label.appendChild(document.createTextNode(labelValue));
  return label;
}
Cell.prototype.getAnchor = function() {
  var anchor = document.createElement("a");
  anchor.onclick = this.row.deletRow;
  anchor.href = "#";
  anchor.row = this.row;
  anchor.appendChild(document.createTextNode("Delete"));
  return anchor;
}

window.onload = function(){
  var recordInputForm = new Form("form", "recordsTable");
  recordInputForm.handleEvents();
  var search = new Search("recordsTable", "searchField");
  search.handleKeyupEvent(); 
}
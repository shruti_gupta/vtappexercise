var page = new CheckAllorNone();

function CheckAllorNone() {}

CheckAllorNone.prototype.checkUncheck = function(booleanValue) {
  var value = booleanValue;
  var checkboxes = document.getElementsByName('check');
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = value;
  }
}


function CheckList(parentDivId){
  this.childivId = parentDivId + "Children";
  this.childDivElement = document.getElementById(this.childivId);
  this.parentCheckbox = document.querySelector("#" + parentDivId + " input[name='main']");

  this.handleClickEvent = function(){  //binding checkbox click event with every parent checkbox
    var callingObject = this ;
    this.parentCheckbox.onchange = function(){
      var childrenCheckboxes = document.querySelectorAll("#" + callingObject.childivId + " input[name='check']");
      for(var i = 0; i < childrenCheckboxes.length; i++){
        childrenCheckboxes[i].checked = callingObject.parentCheckbox.checked;
      }
      callingObject.hideUnhideDiv(callingObject);
    }
  }

  this.hideUnhideDiv = function(callingObject){
    if(callingObject.childDivElement.style.display == "none" || callingObject.childDivElement.style.display == ""){
      callingObject.childDivElement.style.display = "block";
    }else{
      callingObject.childDivElement.style.display = "none";
    } 
    callingObject.parentCheckbox.scrollIntoView(); //scrolls the list for its visibility
  }
}

window.onload = function(){
  var movieChoice = new CheckList("moviesdiv");
  var drinkChoice = new CheckList("drinksdiv");
  var bikeChoice = new CheckList("bikesdiv");
  var colorChoice = new CheckList("colorsdiv");
  movieChoice.handleClickEvent();
  drinkChoice.handleClickEvent();
  bikeChoice.handleClickEvent();
  colorChoice.handleClickEvent();
}

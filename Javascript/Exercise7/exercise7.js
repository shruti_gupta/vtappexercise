function WelcomeUser(){
  this.printingElement = document.getElementById("renderName");
  this.firstname = this.getName("Firstname");
  this.lastname = this.getName("Lastname"); 
}
//check if a valid name his entered
WelcomeUser.prototype.checkName = function(userName, name){
  if(!(isNaN(userName))){
    alert("Please enter a valid " + name);
    return true;
  }
  return false;
}
//method that generates prompt for name input
WelcomeUser.prototype.getName = function(name){
  var userName;
  do{
    userName = prompt("Enter your "+name);
  }while(this.checkName(userName, name))
  return userName.trim();
}
//print valid name on html
WelcomeUser.prototype.printOnHTML = function(){
  var msgString = "Welcome " + this.firstname + " " + this.lastname;
  this.printingElement.appendChild(document.createTextNode(msgString));
}
var user = new WelcomeUser();
user.printOnHTML();
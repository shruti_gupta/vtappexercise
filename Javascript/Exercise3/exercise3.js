var weekdays = new Weekdays();

function Weekdays() {
  this.dayArray = []; //array for maintaing selected weekday's list

  this.maximumChecks = 3;

  this.checkOverflow = function(object) {
    var index = this.dayArray.indexOf(object.value);
    if (object.checked) {
      this.dayArray.push(object.value);

    } else {
      this.dayArray.splice(index, 1); //when checkbox is unchecked
    }

    this.checkArrayLength(object, index);
  }

  this.checkArrayLength = function(object, index){

    if (this.dayArray.length > this.maximumChecks) {
      object.checked = false;
      this.dayArray.splice(index, 1);
      alert("You can select atmost " + this.maximumChecks + " Days. You have already selected " + this.dayArray.join(" , "));
    }

    if (this.dayArray.length > 0) {
      nonecheckbox.checked=false;
    }
  }

  this.noneCheck = function() {

    nonecheckbox.checked = true;
    var checkboxes = document.querySelectorAll('input[name="check"]:checked'); //to select only those checkboxes which are checked
    for (var i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = false;
    }
    this.dayArray = [];
  }
}

function Checkbox(){
  this.checkboxes = document.getElementsByName("check");

  this.nonecheckbox = document.getElementById("nonecheckbox");

  this.handleClickEvent = function(){
    for (var i = 0; i < this.checkboxes.length; i++) {
      this.checkboxes[i].onchange = function(){
        weekdays.checkOverflow(this);
      }
    }
  this.nonecheckbox.onchange = function(){
    weekdays.noneCheck();
    }
  }
}

window.onload = function(){
  var checkBox = new Checkbox();
  checkBox.handleClickEvent();
}
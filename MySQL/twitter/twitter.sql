
/*Find all users with all their followings. */
select a.name as user, b.name as followers 
from users a, users b, relationships c 
where a.id = c.user_id and b.id = c.following_id;

/*
+-------+-----------+
| name  | following |
+-------+-----------+
| Akhil | Manik     |
| Akhil | Amit      |
| Akhil | Kapil     |
| Akhil | Ankur     |
| Akhil | Suman     |
| John  | Ryan      |
| Ryan  | John      |
| Kapil | Akhil     |
| Kapil | Manik     |
| Kapil | Suman     |
| Ankur | Akhil     |
| Ankur | Manik     |
| Ankur | Amit      |
| Ankur | Rahul     |
| Ankur | Kapil     |
| Ankur | John      |
| Ankur | Ryan      |
| Ankur | Sunil     |
| Ankur | Suman     |
+-------+-----------+
19 rows in set (0.01 sec)
*/

/*Find all users with all their followings (comma separated). 
User having maximum followings should be on the top and min at the bottom. */
select a.name as user,group_concat(b.name) as followers 
from users a, users b, relationships c  
where a.id=c.user_id and b.id=c.following_id 
group by a.name 
order by count(a.name) desc;

/*
+-------+----------------------------------------------------+
| user  | followers                                          |
+-------+----------------------------------------------------+
| Ankur | Akhil,Manik,Amit,Rahul,Kapil,John,Ryan,Sunil,Suman |
| Akhil | Manik,Amit,Kapil,Ankur,Suman                       |
| Kapil | Akhil,Manik,Suman                                  |
| John  | Ryan                                               |
| Ryan  | John                                               |
+-------+----------------------------------------------------+

*/

/*Display my timeline(users.id=1). Timeline is tweets by me and my followings in reverse chrono. order.*/


select distinct u.name,content,t.created_at  
from relationships r,tweets t, users u  
where r.user_id = 1  
and (r.user_id = t.user_id or r.following_id = t.user_id)
and t.user_id = u.id
order by t.created_at desc;


/*
+-------+-------------------------------------------------+---------------------+
| name  | content                                         | created_at          |
+-------+-------------------------------------------------+---------------------+
| Akhil | Its raining                                     | 2012-08-07 12:57:43 |
| Manik | Stop playing twitter twitter, focus on your job | 2012-08-07 12:55:46 |
| Ankur | Thanks for letting me know                      | 2012-08-07 12:54:51 |
| Ankur | Nokis is a nice mobile company                  | 2012-08-07 12:54:20 |
| Kapil | I am at vinsol                                  | 2012-08-07 12:53:59 |
| Suman | Lets test this...                               | 2012-08-07 12:53:18 |
| Akhil | lorem ipsum                                     | 2012-08-07 12:53:01 |
| Manik | Hello there                                     | 2012-08-07 12:52:46 |
| Manik | Test tweet.                                     | 2012-08-07 12:52:38 |
| Akhil | My first ever tweet.                            | 2012-08-07 12:52:11 |
+-------+-------------------------------------------------+---------------------+

*/

/*Findout how many tweets each users has done */
select name, count(user_id) as tweets 
from users, tweets 
where tweets.user_id = users.id 
group by user_id;
/*
+------------------+----------------+
|       name       | count(content) |
+------------------+----------------+
| Akhil            |              3 |
| Manik            |              3 |
| Rahul            |              1 |
| Kapil            |              1 |
| John             |              1 |
| Ryan             |              2 |
| Sunil            |              1 |
| Ankur            |              2 |
| Suman            |              1 |
+------------------+----------------+
*/

/*Findout all users who haven’t tweeted ever*/

select name 
from tweets right outer join users 
on users.id = tweets.user_id 
where user_id is null;

/*
+------+
| name |
+------+
| Amit |
+------+
*/

/*Findout all users with their tweets who has tweeted in last 1 hour*/
select name as user, content as tweets 
from users, tweets 
where users.id = tweets.user_id 
and created_at > date_sub(now(),interval 1 hour);
/*What all cities did people respond from*/
select distinct city from Campaign;

/*How many people responded from each city*/
select count(email),city  from Campaign group by city;

/*Which city were the maximum respondents from*/
select count(email) as peopleCount, city 
from Campaign group by city 
having peopleCount = 
(select max(peopleCount) 
  from 
  (
    select count(email) as peopleCount 
    from Campaign group by city
  )as t
);

/*What all email domains did people respond from*/
select distinct(substring_index(email, '@', -1)) from Campaign;

/*Which is the most popular email domain among the respondents */
select  distinct count(substring_index(email,'@',-1)) as domainCount, substring_index(email,'@',-1) as domain
from Campaign
group by domain 
having domainCount = 
( select max(domainCount) 
  from  
  ( select distinct count(substring_index(email,'@',-1)) as domainCount, substring_index(email,'@',-1) as domain  
    from Campaign group by domain 
  ) as t
);
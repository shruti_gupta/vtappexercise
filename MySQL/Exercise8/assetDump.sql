-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: AssetMgmt
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ALLOTMENT`
--

DROP TABLE IF EXISTS `ALLOTMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALLOTMENT` (
  `emp_id` varchar(5) DEFAULT NULL,
  `asset_id` varchar(20) DEFAULT NULL,
  `allotment_Date` date DEFAULT NULL,
  `return_Date` date DEFAULT NULL,
  UNIQUE KEY `allotment_idx` (`emp_id`,`asset_id`),
  KEY `asset_id` (`asset_id`),
  CONSTRAINT `ALLOTMENT_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `EMPLOYEES` (`id`),
  CONSTRAINT `ALLOTMENT_ibfk_2` FOREIGN KEY (`asset_id`) REFERENCES `ASSETS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALLOTMENT`
--

LOCK TABLES `ALLOTMENT` WRITE;
/*!40000 ALTER TABLE `ALLOTMENT` DISABLE KEYS */;
INSERT INTO `ALLOTMENT` VALUES ('a1','l1','2011-01-01','2011-12-31'),('b1','l2','2011-01-01','2011-12-31'),('hr1','l3','2014-06-01',NULL),('hr1','l4','2014-06-01',NULL),('a1','ip1','2011-04-01',NULL),('b1','ip2','2011-01-01',NULL),('hr1','l1','2011-12-31',NULL),('hr1','l2','2011-12-31',NULL);
/*!40000 ALTER TABLE `ALLOTMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ASSETS`
--

DROP TABLE IF EXISTS `ASSETS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ASSETS` (
  `id` varchar(5) NOT NULL,
  `asset_name` varchar(10) DEFAULT NULL,
  `purchased_on` date DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `location` varchar(15) DEFAULT NULL,
  `ifPublic` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `asset_idx` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ASSETS`
--

LOCK TABLES `ASSETS` WRITE;
/*!40000 ALTER TABLE `ASSETS` DISABLE KEYS */;
INSERT INTO `ASSETS` VALUES ('ip1','iphone A','2011-04-01','2011',30000,NULL,0),('ip2','iphone B','2011-01-01','2011',30000,NULL,0),('l1','laptop A','2011-01-01','2011',30000,NULL,0),('l2','laptop B','2011-01-01','2011',30000,NULL,0),('l3','laptop N1','2014-06-01','2014',30000,NULL,0),('l4','laptop N2','2014-06-01','2014',30600,NULL,0),('pri1','printer A','2011-08-15','2011',6000,'meeting room',1),('pri2','printer B','2011-09-10','2011',6000,NULL,0),('pro1','projector ','2011-08-15','2011',5000,'meeting room',1);
/*!40000 ALTER TABLE `ASSETS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EMPLOYEES`
--

DROP TABLE IF EXISTS `EMPLOYEES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMPLOYEES` (
  `id` varchar(5) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_idx` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EMPLOYEES`
--

LOCK TABLES `EMPLOYEES` WRITE;
/*!40000 ALTER TABLE `EMPLOYEES` DISABLE KEYS */;
INSERT INTO `EMPLOYEES` VALUES ('a1','Alice'),('b1','Bob'),('c1','Chris'),('d1','Duke'),('e1','Emily'),('hr1','Harry');
/*!40000 ALTER TABLE `EMPLOYEES` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-06 13:56:54

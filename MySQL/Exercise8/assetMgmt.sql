/*Find the name of the employee who has been alloted the maximum number of assets till date*/
select count(asset_id) as assetCount, name 
from ALLOTMENT inner join EMPLOYEES 
on ALLOTMENT.emp_id = EMPLOYEES.id 
group by emp_id 
having assetCount = 
(
  select count(asset_id) as assetCount 
  from ALLOTMENT inner join EMPLOYEES 
  on ALLOTMENT.emp_id = EMPLOYEES.id 
  group by emp_id 
  order by assetCount 
  desc limit 1
);

/*Identify the name of the employee who currently has the maximum number of assets as of today*/

select count(asset_id) as assetCount, name 
from ALLOTMENT inner join EMPLOYEES 
on ALLOTMENT.emp_id = EMPLOYEES.id
where return_Date is null
group by emp_id 
having assetCount = 
(
  select count(asset_id) as assetCount 
  from ALLOTMENT inner join EMPLOYEES 
  on ALLOTMENT.emp_id = EMPLOYEES.id
  where return_Date is null
  group by emp_id 
  order by assetCount 
  desc limit 1
);

/*Find name and period of all the employees who have used a Laptop - 
let’s say laptop A - since it was bought by the company*/

select name, asset_name, datediff(return_Date, allotment_Date)/365 as `period of usage(years)`  
from ASSETS, ALLOTMENT, EMPLOYEES 
where ASSETS.id = ALLOTMENT.asset_id    
and asset_name = "laptop A" 
and EMPLOYEES.id = ALLOTMENT.emp_id;

/*Find the list of assets that are currently not assigned to anyone hence lying with the asset manage ( HR)*/
 select asset_name 
 from ALLOTMENT, ASSETS 
 where ASSETS.id = ALLOTMENT.asset_id 
 and emp_id = 'hr1';

/*An employee say Bob is leaving the company, 
write a query to get the list of assets he should be returning to the company*/
sselect asset_name 
from ASSETS ast,ALLOTMENT alt, EMPLOYEES emp 
where emp.name = 'Bob' 
and alt.emp_id = emp.id 
and alt.asset_id = ast.id 
and return_Date is null;

/*Write a query to find assets which are out of warranty (Do using subquery also)*/
select asset_name from ASSETS where datediff(NOW(), purchased_on)/365 > 1;

/*Return a list of Employee Names who do not have any asset assigned to them (Do using subquery also).*/
/*subquery*/
select name from EMPLOYEES where id not in (select distinct emp_id from ALLOTMENT );

/*without subquery*/
select name from ALLOTMENT right outer  join EMPLOYEES on EMPLOYEES.id = ALLOTMENT.emp_id where asset_id is null;
/*Find image that has been tagged most no of times*/

select count(tagged) as tagcount, image_id
from TAGS
group by image_id
having tagCount =
(
  select count(tagged) as tagcount
  from TAGS
  group by image_id
  order by tagCount desc limit 1
);

/*Find all images belonging to the friends of a particular user*/
select u1.name as user, u2.name as friend, i.id as Images
from USERS u1, USERS u2, FRIENDS f, IMAGES i
where u1.id = f.user_id
and u2.id = f.friend
and f.friend = i.image_user;

/*Find all friends of a particular user (Say, userA) who has tagged him in all of his pics.*/
select user, friend from(
  (select count(id) as imageCount,image_user from IMAGES group by image_user) as images
inner join
  (
    select u1.name as user, u2.name as friend,u2.id as friendId, count(i.id) as friendImageCount
    from FRIENDS, USERS u1, USERS u2, IMAGES i ,TAGS t
    where u1.id = FRIENDS.user_id
    and u2.id = FRIENDS.friend
    and u1.name = 'asim'
    and u2.id = i.image_user
    and i.id = t.image_id
    group by u2.name
  ) as userfriends
)
where images.image_user = userfriends.friendId
and imageCount = friendImageCount;

/*Find friend of a particular user (Say, userA) who have tagged him most no. of times.*/

 select distinct u1.name as `user`, u2.name as `friend who has tagged user the most`, count(i.id) as tag_count
 from FRIENDS f, USERS u1, USERS u2, IMAGES i, TAGS t
 where u1.name = 'asim'
 and  u1.id = f.user_id
 and f.friend = u2.id
 and i.image_user = f.friend
 and i.id = t.image_id
 and t.tagged = u1.id
 group by u2.id
 having tag_count =
 (
    select count(i.id) as tag_count
    from FRIENDS f, USERS u1, USERS u2, IMAGES i, TAGS t
    where u1.name = 'asim'
    and  u1.id = f.user_id
    and f.friend = u2.id
    and i.image_user = f.friend
    and i.id = t.image_id
    and t.tagged = u1.id
    order by tag_count desc limit 1
  );
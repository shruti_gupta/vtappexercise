/*create user*/
create user 'vtapp_user'@'localhost' identified by '1234';

/*grant permission to access vtapp DB*/
grant all privileges on vtapp.* to 'vtapp_user'@'localhost';
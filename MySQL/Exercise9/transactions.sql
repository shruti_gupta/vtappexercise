Q1)

/*add 1000 rs to user account andy*/
update ACCOUNTS set balance = balance + 1000 
  where account_no in
    (select account_no from USERS where name = 'andy');


Q2)

/*add 1000 rs to user account andy*/
update ACCOUNTS set balance = balance - 500 
  where account_no in
  (select account_no from USERS where name = 'andy');

Q3)
start transaction;
set @@autocommit = 0;

/*initial account state for user andy and bob
+-------+------------+---------+
| id    | account_no | balance |
+-------+------------+---------+
| cust1 | 12345      |   21000 | <--andy
| cust2 | 123456     |  200400 | <-- bob
| cust3 | 1234567    |  300400 |
+-------+------------+---------+

*/
--case 1
update ACCOUNTS set balance = balance - 200 
  where account_no in
  (select account_no from USERS where name = 'andy');

update ACCOUNTS set balance = balance + 200 
  where account_no in   
  (select account_no from USERS where name = 'bob');

/*account state after execution of the above queries
+-------+------------+---------+
| id    | account_no | balance |
+-------+------------+---------+
| cust1 | 12345      |   20800 | <--andy
| cust2 | 123456     |  200600 | <-- bob
| cust3 | 1234567    |  300400 |
+-------+------------+---------+
*/

rollback;

/*account state after rollback 
+-------+------------+---------+
| id    | account_no | balance |
+-------+------------+---------+
| cust1 | 12345      |   21000 | <--andy
| cust2 | 123456     |  200400 | <-- bob
| cust3 | 1234567    |  300400 |
+-------+------------+---------+

the changes made by both the queries have been rolled back
*/

--case2

update ACCOUNTS set balance = balance - 200 
  where account_no in
  (select account_no from USERS where name = 'andy');

update ACCOUNTS set balance = balance + 200 
  where account_no in   
  (select account_no from USERS where name = 'bob');

/*account state after execution of above query
+-------+------------+---------+
| id    | account_no | balance |
+-------+------------+---------+
| cust1 | 12345      |   20800 | <--andy
| cust2 | 123456     |  200600 | <-- bob
| cust3 | 1234567    |  300400 |
+-------+------------+---------+
*/

commit;

rollback;

/*if we try to rollback the changes..this would not be effective
+-------+------------+---------+
| id    | account_no | balance |
+-------+------------+---------+
| cust1 | 12345      |   20800 | <--andy
| cust2 | 123456     |  200600 | <-- bob
| cust3 | 1234567    |  300400 |
+-------+------------+---------+
*/

set @@autocommit = 1;

/*the balance is now transfered to bob's account*/
create function calcArea (shape varchar(20), side1 int, side2 int) returns float
  begin
  declare area float default 0.00;
  case shape 
  when 'triangle' then set area = sqrt(3)/4 * pow(side1,2);
  when 'circle' then set area = pi() * pow(side1,2);
  when 'rectangle' then set area = side1 * side2;
  else set area = side1 * side1;
  end case;
  return area;
  end;
  //

Q1)

select shape, color, truncate(calcArea(shape, side1,side2),2) as area from blocks;

/*output:
+-----------+--------+--------+
| shape     | color  | area   |
+-----------+--------+--------+
| triangle  | red    | 10.83  |
| square    | red    | 25.00  |
| rectangle | red    | 8.00   |
| circle    | red    | 78.54  |
| circle    | blue   | 12.57  |
| square    | blue   | 49.00  |
| square    | yellow | 64.00  |
| triangle  | yellow | 1.73   |
| triangle  | blue   | 3.90   |
| circle    | green  | 314.16 |
| square    | green  | 1.00   |
| square    | cyan   | 16.00  |
| rectangle | cyan   | 56.00  |
| rectangle | yellow | 10.00  |
| rectangle | white  | 20.00  |
| circle    | white  | 50.27  |
+-----------+--------+--------+

*/

Q2)

select shape, color,truncate(sum(calcArea(shape, side1,side2)),2) as area from blocks group by shape;

/*output:
+-----------+-------+--------+
| shape     | color | area   |
+-----------+-------+--------+
| circle    | red   | 455.53 |
| rectangle | red   | 94.00  |
| square    | red   | 155.00 |
| triangle  | red   | 16.45  |
+-----------+-------+--------+
*/

Q3)
select shape, color, truncate(ifnull((qty * area), 0.00),2) as area from
  block_qty right outer join 
  ( 
    select id,shape,color,calcArea(shape, side1,side2) as area from blocks
  )as t
 on t.id = block_qty.block_id

/* output:
+-----------+--------+----------+
| shape     | color  | area     |
+-----------+--------+----------+
| triangle  | red    | 75.78    |
| square    | red    | 125.00   |
| rectangle | red    | 24.00    |
| circle    | red    | 157.08   |
| circle    | blue   | 0.00     |
| square    | blue   | 245.00   |
| square    | yellow | 320.00   |
| triangle  | yellow | 13.86    |
| triangle  | blue   | 15.59    |
| circle    | green  | 2513.27 |
| square    | green  | 8.00     |
| square    | cyan   | 96.00    |
| rectangle | cyan   | 336.00   |
| rectangle | yellow | 10.00    |
| rectangle | white  | 140.00   |
| circle    | white  | 0.00     |
+-----------+--------+----------+

*/

Q4)
select color, truncate(sum((qty * area)),2) as area from
  block_qty right outer join 
  ( 
    select id,shape,color,calcArea(shape, side1,side2) as area from blocks
  )as t
 on t.id = block_qty.block_id
group by color
having area >= 15*20;

/*output
+--------+---------+
| color  | area    |
+--------+---------+
| cyan   |  432.00 |
| green  | 2521.27 |
| red    |  381.85 |
| yellow |  343.85 |
+--------+---------+
*/


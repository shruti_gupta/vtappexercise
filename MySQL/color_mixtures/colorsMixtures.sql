
/*Find the colors that can be clubbed with 'Red' and also name the resulting color*/
select c1.name color1, c2.name color2, c3.name mixture 
from colors c1, colors c2, colors c3, mixtures m 
where c1.id = m.parent1_id 
and c2.id  = m.parent2_id 
and c3.id = m.mix_id 
and (c1.name = 'Red' or c2.name = 'Red');

/*
+----------------------+----------------------+------------------+
|       color1         |        color2        |      mixture     |
+----------------------+----------------------+------------------+
| Red                  | Green                | Yellow           |
| Red                  | Blue                 | Pink             |
| Red                  | Yellow               | White            |
+----------------------+----------------------+------------------+
*/

/*Find mixtures that can be formed without 'Red'*/
select c1.name color1, c2.name color2, c3.name mixture
from colors c1, colors c2, colors c3, mixtures m 
where c1.id = m.parent1_id 
and c2.id  = m.parent2_id 
and c3.id = m.mix_id 
and (c1.name <> 'Red' and c2.name <> 'Red');

/*
+-------+------+------+
| name  | name | name |
+-------+------+------+
| Green | Blue | Cyan |
+-------+------+------+
*/

/*Find the mixtures that have one common parent*/

select colors.name as `parent name`,group_concat(temp.name separator ' & ') as `possible mixture`
from mixtures inner join colors on ( colors.id = parent1_id || colors.id = parent2_id ) 
inner join colors as temp on (temp.id = mixtures.mix_id) 
group by colors.id;

/*Find parent colors(as a couple) that give mix colors with density higher than the color density originally*/
select concat_ws('&',parent1_id, parent2_id) as parent_colors,
mix_id,format(mix_density,2) as mix_density,format(c2.density,2) as original_density 
from mixtures m, colors c ,colors c2 
where (c.id = m.parent1_id or c.id = m.parent2_id) 
and (c2.id = m.mix_id) 
and m.mix_density > c.density 
group by mix_id;

/*
+---------------+--------+-----------------------+------------------+
| parent_colors | mix_id |      mix_density      | original_density |
+---------------+--------+-----------------------+------------------+
| 10&11         |     13 | 0.60                  | 0.20             |
| 10&12         |     14 | 0.50                  | 0.30             |
| 11&12         |     15 | 0.75                  | 0.40             |
| 10&13         |     16 | 0.28                  | 0.28             |
+---------------+--------+-----------------------+------------------+

*/

/*calculate the total amount of color 'Red'(in kgs) needed to make a 1kg mix each for 
its possible mixtures(yellow,pink..)*/

create function calcAmount(parent1_perc float, parent2_perc float, colorId int, parent1_id int, parent2_id int) 
  returns float 
  begin 
  declare amount float default 0.00; 
  set amount = if(parent1_id = colorId, parent1_perc/100, parent2_perc/100); 
  return amount; 
  end;
  //

select truncate(sum(calcAmount(parent1_perc,parent2_perc,10,parent1_id,parent2_id)),1) as amount 
from mixtures m, colors c1, colors c2  
where c1.id = m.parent1_id 
and c2.id  = m.parent2_id 
and 'Red' in (c1.name, c2.name);

/*
+--------+
| amount |
+--------+
|   1.6  |
+--------+
*/
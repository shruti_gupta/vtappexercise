/*select articles written by user3*/

 select distinct Article, User 
 from USERS,ARTICLES 
 where ARTICLES.User_id = USERS.User_id
 and USERS.User = 'user3';

 /*with subquery*/
 select Article 
 from ARTICLES 
 where User_id in 
 (select User_id from USERS where User = 'user3');

/*select articles and comments for above*/

/*without subquery*/
select distinct Article,Comment 
from ARTICLES, COMMENTS, USERS 
where USERS.User = 'user3' 
and ARTICLES.User_id = USERS.User_id
and ARTICLES.Article_id = COMMENTS.Article_id;

/*subquery*/
select Article_id, Comment 
from COMMENTS 
where Article_id in 
(select Article_id 
  from ARTICLES 
  where ARTICLES.User_id in 
  (select User_id 
    from USERS 
    where User = 'user3'
  )
);

/*select articles which have no comments*/
select Article f
rom ARTICLES,COMMENTS
where ARTICLES.Article_id = COMMENTS.Article_id 
and Comment is null;

/*select max of comments for an article*/
select  Article, count(distinct(Comment)) as cmntCount
from COMMENTS, ARTICLES 
where ARTICLES.Article_id = COMMENTS.Article_id 
group by Article
having cmntCount = 
  (select count(distinct(Comment)) as cmntCount
    from COMMENTS, ARTICLES 
    where ARTICLES.Article_id = COMMENTS.Article_id 
    group by Article
    order by cmntCount desc limit 1
  );

/*to select article which does not have more than one comment by the same user*/
select Article 
from ARTICLES,COMMENTS 
where ARTICLES.Article_id = COMMENTS.Article_id 
group by Article 
having count(distinct(CommentBy)) >= count(Comment);
/*creating table in database testing*/
create table testing.testing_table (name varchar(30),contact_name varchar(30), roll_no varchar(10) );

/*deleting column name*/
alter table testing_table drop name;

/*rename contact_name to user name*/
alter table testing_table change contact_name username varchar(30);

/*add two fields first name and last name*/
alter table testing_table add (first_name varchar(20), last_name varchar(20));

/*change type of roll_no*/
alter table testing_table modify roll_no int(20);
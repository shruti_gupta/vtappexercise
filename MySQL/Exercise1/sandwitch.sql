/*select places where jones can eat(with subquery)*/
select Location 
from Sandwitches 
where Filling in (select Filling from Tastes where Name = 'Jones');

/*select places where jones can eat(without subquery)*/
select Location 
from Sandwitches inner join Tastes 
on Sandwitches.Filling = Tastes.Filling 
where Name = 'Jones';

/*for each location the number of ppl who can eat there*/
select Location, count(Tastes.Name) 
from Tastes inner join Sandwitches 
on Tastes.Filling = Sandwitches.Filling 
group by Sandwitches.Location; 
/*select names of library books of publisher macmilan*/
select Title from Titles where Publisher like '%Macmilan%';

/*branches tht hold any book by ann brown(with subquery)*/
select distinct Branch from Holdings where Title in (select Title from Titles where Author = 'Ann Brown');

/*branches tht hold any book by ann brown(without subquery)*/
select distinct Holdings.Branch from Holdings inner join Titles on Holdings.Title = Titles.Title;

/*total no. of books held at each branch*/
select sum(copies) from Holdings group by Branch;